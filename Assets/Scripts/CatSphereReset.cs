﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatSphereReset : MonoBehaviour {
    public MeshFilter normalCatSphere;
    public MeshFilter myMesh;
    public MeshFilter previousSphere;
    public float counter = 10;
    float timeUntilChange = 0.75f;
    bool changed = false;
    void Start () {
        previousSphere.sharedMesh = myMesh.sharedMesh;
    }
	
	void Update () {
        if(myMesh.sharedMesh != previousSphere.sharedMesh)
        {
            counter = 0;
            previousSphere.sharedMesh = myMesh.sharedMesh;
            //Debug.Log("set counter");
        }
        if (myMesh.sharedMesh != normalCatSphere.sharedMesh)
        {
            counter += Time.deltaTime;
            if (counter > timeUntilChange)
            {
                //Debug.Log("return tha mesh");
                myMesh.mesh = normalCatSphere.sharedMesh;
            }
        }

    }
}
