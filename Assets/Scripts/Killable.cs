using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
public class Killable : MonoBehaviour
{
    public RuntimeAnimatorController transitionController;
    public ParticleSystem dieParticles;
    public GameObject characterVisualsToDisable;
    public GameObject earphonesLeftOver;
    GameObject earphones;
    Vector3 randomDir;
    Vector3 randomAngle;
    bool dead = false;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponentInParent<Killer>() != null && !dead)
        {
            StartCoroutine(Die());
            dieParticles.Emit(50);
            gameObject.GetComponent<PlayerInput>().enabled = false;
            ChangeVisuals();
            Animator[] anims = other.gameObject.GetComponentsInChildren<Animator>();
            foreach (Animator an in anims)
            {
                if (an.runtimeAnimatorController == transitionController)
                {
                    Debug.Log("trigger");
                    an.SetTrigger("Close");
                }
            }
            dead = true;
        }
    }
    private void Start()
    {
        randomDir = new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), 0);
        randomAngle = new Vector3(0, 0, Random.Range(-180, 180));
    }
    private void Update()
    {
        if (earphones != null)
        {
            earphones.transform.position += randomDir.normalized * 0.01f;
            earphones.transform.Rotate(randomAngle/100);
        }
    }
    IEnumerator Die()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ChangeVisuals()
    {
        earphones = Instantiate(earphonesLeftOver, transform);
        earphones.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //earphones.AddComponent<CircleCollider2D>().radius = 1;

        characterVisualsToDisable.SetActive(false);
    }
}