﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LevelName : MonoBehaviour {
    Text txt;

    void Start()
    {
        txt = GetComponent<Text>();
    }


    void Update()
    {
        txt.text = SceneManager.GetActiveScene().name;        
    }
}
