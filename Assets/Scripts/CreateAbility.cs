﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateAbility : MonoBehaviour {
    public Material meshRen;
    public GameObject ediblePrefab;
    //public KeyCode inputForCreate = KeyCode.S;
    Animator anim;
    public MeshFilter eatingCatSphere;
    public MeshFilter sphereToChange;
    public ParticleSystem particleSystem;
    void Start()
    {
        meshRen = GetComponentInChildren<MeshRenderer>().material;
        anim = GetComponentInChildren<Animator>();
    }

    void Update () {
        if (Input.GetButtonDown("Fire2"))
        {
            //Debug.Log("Fire2");
            meshRen.SetColor("_V_WIRE_Color", Color.blue);
            Create();
        }
        if (Input.GetButtonUp("Fire2"))
        {
            meshRen.SetColor("_V_WIRE_Color", Color.white);
        }
    }

    void Create()
    {
        if(GlobalState.boxesEaten > 0)
        {
            GlobalState.boxesEaten--;
            GameObject obj = Instantiate(ediblePrefab);
            obj.transform.position = transform.position - Vector3.up;

            anim.SetTrigger("Puke");
            sphereToChange.sharedMesh = eatingCatSphere.sharedMesh;
            particleSystem.Emit(30);
        }
    }
}
