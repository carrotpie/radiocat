﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayGenerator : MonoBehaviour {
    public GameObject ray;
    float counter;
    public float interval = 4;
    int flip = 1;
	void Start () {
		
	}
	
	void Update () {
        counter += Time.deltaTime;
        if(counter> interval)
        {
            counter = 0;
            GameObject newRay = Instantiate(ray, transform);
            newRay.GetComponent<Ray>().direction = (RayDirection)flip;
            if (flip == 0)
                flip = 1;
            else
                flip = 0;
        }
	}
}
