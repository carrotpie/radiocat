﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GlowBlinkFx : MonoBehaviour
{
    public Color firstColor;
    public Color secondColor;
    Color aColor = Color.black;
    Color bColor = Color.white;
    public float duration = 1f; // duration in seconds	
    public bool disableParent;

    float t;
    bool ab;

    void OnEnable()
    {
        disableParent = false;
        aColor = firstColor; // heavy upgrade here. might ruin smth
        bColor = secondColor;
    }

    void Update()
    {
        if (ab)
        {
            if (GetComponent<RawImage>() != null)
                GetComponent<RawImage>().color = Color.Lerp(aColor, bColor, t);
            if (GetComponent<Image>() != null)
                GetComponent<Image>().color = Color.Lerp(aColor, bColor, t);
            if (GetComponent<Text>() != null)
                GetComponent<Text>().color = Color.Lerp(aColor, bColor, t);
            if (GetComponent<Renderer>() != null)
                GetComponent<Renderer>().materials[0].color = Color.Lerp(aColor, bColor, t);
        }
        else
        {
            if (GetComponent<RawImage>() != null)
                GetComponent<RawImage>().color = Color.Lerp(bColor, aColor, t);
            if (GetComponent<Image>() != null)
                GetComponent<Image>().color = Color.Lerp(bColor, aColor, t);
            if (GetComponent<Text>() != null)
                GetComponent<Text>().color = Color.Lerp(bColor, aColor, t);
            if (GetComponent<Renderer>() != null)
                GetComponent<Renderer>().materials[0].color = Color.Lerp(bColor, aColor, t);
        }
        if (t < 1)
        { // while t below the end limit...
          // increment it at the desired rate every update:
            t += Time.deltaTime / duration;
        }
        else
        {
            if (disableParent)
            {//disableParent = false;
                transform.parent.gameObject.SetActive(false);
            }
            t = 0;
            ab = !ab;
        }
    }
    void OnDisable()
    {
        if (GetComponent<RawImage>() != null)
            GetComponent<RawImage>().color = aColor;
        if (GetComponent<Image>() != null)
            GetComponent<Image>().color = aColor;
        if (GetComponent<Text>() != null)
            GetComponent<Text>().color = aColor;
        if (GetComponent<Renderer>() != null)
            GetComponent<Renderer>().materials[0].color = aColor;
        t = 0;
        ab = true;
    }
    //call this to engage fadeout, then it disables the parent of this object.
    public void FadeOutAndDisableParent()
    { //heavy upgrde here, might ruin smth
        disableParent = true;
        ab = true;
        t = 0;
        if (GetComponent<RawImage>() != null)
            aColor = GetComponent<RawImage>().color;
        if (GetComponent<Image>() != null)
            aColor = GetComponent<Image>().color;
        if (GetComponent<Text>() != null)
            aColor = GetComponent<Text>().color;
        bColor = new Color(0, 0, 0, 0);
    }
    public void SetDuration(bool value)
    {
        if (value)
        {
            duration = 0.3f;
        }
        else
        {
            duration = 3f;
        }

    }
    public void testBlink()
    {
        Debug.Log("Test");
    }
}