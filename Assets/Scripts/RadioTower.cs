﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class RadioTower : MonoBehaviour {
    public RuntimeAnimatorController transitionController;
    public GameObject winPanelPref;
    void OnTriggerEnter2D(Collider2D collider)
    {         
        if (collider.tag == "Player")
        {
            if (SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings)
            {
                StartCoroutine(LoadSceneAfterDelay());
                Animator[] anims = collider.gameObject.GetComponentsInChildren<Animator>();
                foreach(Animator an in anims)
                {
                    if (an.runtimeAnimatorController == transitionController)
                    {
                        //Debug.Log("trigger");
                        an.SetTrigger("Close");
                    }
                }
            }
            else
                Victory();
        }
    }

    IEnumerator LoadSceneAfterDelay()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    void Victory()
    {
        GameObject winPan = Instantiate(winPanelPref, FindObjectOfType<Canvas>().transform);
        //.GetComponentInChildren<Image>().gameObject.SetActive(true);
    }
}
