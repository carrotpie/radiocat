﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ray : MonoBehaviour {
    public RayDirection direction = RayDirection.right;
    float speed = 0.1f;
    private void Start()
    {
        Destroy(gameObject, 30);
    }
    void FixedUpdate() {
        Vector3 dir = Vector3.zero;
        Vector3 rot = Vector3.zero;
        if (direction == RayDirection.right)
        {
            dir = Vector3.right;
            rot = -Vector3.forward;
        }
        else
        {
            dir = Vector3.left;
            rot = Vector3.forward;
        }
        transform.position += dir * speed;
        transform.Rotate (rot * speed * 0.3f);
    }
}
public enum RayDirection { Left,right }
