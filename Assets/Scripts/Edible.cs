﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edible : MonoBehaviour {
    public Transform debriPrefab;

    public void GetEaten(Transform mouth)
    {
        for (int i = 0; i < 8; i++)
        {
            float axisOffset = 0.25f;
            Vector3 offset = new Vector3(
                i % 2 == 0 ? -1 : 1,
                i / 2 % 2 == 0 ? -1 : 1,
                i / 4 == 0 ? -1 : 1
            ) * axisOffset;

            Transform t = Instantiate(debriPrefab, transform.position + offset, Quaternion.identity);

            t.GetComponent<EdibleDebri>().SetMouth(mouth);
        }

        Destroy(gameObject);
    }
}
