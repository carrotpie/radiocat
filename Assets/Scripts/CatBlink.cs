﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatBlink : MonoBehaviour {

    Animator anim;
    public float counter = 0;
    public float period = 1;
    public int blinksInTenSec = 3;
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        counter += Time.deltaTime;
        if(counter > period)
        {
            if(Random.Range(0, 10) < blinksInTenSec)
            {
                anim.SetTrigger("Blink");
            }
            counter = 0;
        }
    }
}
