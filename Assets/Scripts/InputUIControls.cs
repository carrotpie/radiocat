﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputUIControls : MonoBehaviour {
    public GameObject arrowStart;
    public GameObject arrowKeyboard;
    public GameObject arrowController;

    public GameObject controllerInfo;
    public GameObject keyboardInfo;
    public GameObject contrSelect;
    public GameObject keyboardSelect;

    int tracker = 1;
    void Start () {
		
	}
    private bool m_isAxisInUse = false;

    void Update()
    {
        float input = Input.GetAxisRaw("Vertical");

        if (input != 0)
        {
            if (m_isAxisInUse == false)
            {
                // Call your event function here.
                m_isAxisInUse = true;
                ChoseOther(input);
            }
        }
        if (input == 0)
        {
            m_isAxisInUse = false;
        }
        
        /*if (input != 0)
        {
            ChoseOther(input);
        }*/

        if (Input.GetButtonDown("Submit"))
        {
            StartGame();
        }
    }
    void StartGame()
    {
        keyboardInfo.SetActive(false);
        keyboardSelect.SetActive(false);
        controllerInfo.SetActive(false);
        contrSelect.SetActive(false);
        if(tracker == 1)
        {
            keyboardInfo.SetActive(true);
            keyboardSelect.SetActive(true);
        }
        if (tracker == 2)
        {
            controllerInfo.SetActive(true);
            contrSelect.SetActive(true);
        }
        if (tracker == 3)
            gameObject.SetActive(false);
    }
    void ChoseOther(float input)
    {
        Debug.Log("chose other");
        if (input < 0)
            tracker++;
        else
            tracker--;
        if (tracker > 3)
            tracker = 1;
        if (tracker < 1)
            tracker = 3;
            TrackArrow();

    }
    void TrackArrow()
    {
        arrowKeyboard.SetActive(false);
        arrowController.SetActive(false);
        arrowStart.SetActive(false);
        if (tracker == 1)
        {
            arrowKeyboard.SetActive(true);
        }
        if (tracker == 2)
        {
            arrowController.SetActive(true);
        }
        if (tracker == 3)
        {
            arrowStart.SetActive(true);
        }
    }




}
