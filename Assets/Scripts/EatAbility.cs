﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class EatAbility : MonoBehaviour {
    public bool eatActivated = false;
    public Material meshRen;
    bool collided = false;
    GameObject targetObject;
    //public KeyCode inputToEnableEat;
    Animator anim;
    public MeshFilter eatingCatSphere;
    public MeshFilter sphereToChange;
    void Start () {
        meshRen = GetComponentInChildren<MeshRenderer>().material;
        anim = GetComponentInChildren<Animator>();
    }

    void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            //Debug.Log("fire1");
            eatActivated = true;
            meshRen.SetColor("_V_WIRE_Color", Color.green);
        }
        if (Input.GetButtonUp("Fire1"))        
        {
            eatActivated = false;
            meshRen.SetColor("_V_WIRE_Color", Color.white);
        }
        if (collided)
        {
            Eat(targetObject);
            collided = false;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (eatActivated)
        {
            collided = true;
            targetObject = collider.gameObject;
            //Debug.Log("its edible");
        }
    }

    public void Eat(GameObject target)
    {
        if (!target.GetComponentInParent<Edible>())
            return;
        GlobalState.boxesEaten += 1;      
        target.GetComponentInParent<Edible>().GetEaten(transform);
        Collider2D[] colliders = target.transform.parent.GetComponentsInChildren<Collider2D>();
        foreach (Collider2D col in colliders)
            col.enabled = false;
        anim.SetTrigger("Eat");
        sphereToChange.sharedMesh = eatingCatSphere.sharedMesh;
    }
}
