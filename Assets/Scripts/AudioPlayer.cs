﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour {

    public static AudioPlayer audioPlayer;

    void Awake()
    {

        if (audioPlayer == null)
        {

            DontDestroyOnLoad(gameObject);
            audioPlayer = this;
        }
        else if (audioPlayer != this)
        {
            Destroy(gameObject);
        }

    }
    /*void Start () {
        GameObject go = FindObjectOfType<AudioPlayer>().gameObject;
        if (go != null)
            Destroy(gameObject);
        else
            DontDestroyOnLoad(gameObject);
	}*/
}
