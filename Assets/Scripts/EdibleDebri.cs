﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdibleDebri : MonoBehaviour {
	public float flySpeed = 2;
	public float flyDuration = 0.5f;

	private Transform mouth;
	private Vector3 initialPos;
	private Vector3 initialSpeed;
	private float flyStart;

	public void SetMouth(Transform mouth_)
	{
		mouth = mouth_;

        initialPos = transform.position;
		initialSpeed = Random.onUnitSphere;
		flyStart = Time.time;
	}

	void Update()
	{
		if (Time.time > flyStart + flyDuration)
		{
			Destroy(gameObject);
			return;
		}

		Vector3 pos = initialPos + initialSpeed * flySpeed * (Time.time - flyStart);
		float progress = (Time.time - flyStart) / flyDuration;
		pos = Vector3.Lerp(pos, mouth.position + Vector3.forward * -2, progress);
		pos = Vector3.Lerp(pos, mouth.position, progress);
		transform.position = pos;
	}
}
